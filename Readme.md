# eRAMS 2.0 Coding Style Guides

We implement a style guide for our code with the intention of keeping things readable and consistent. Please do your part on the team to help keep the spirit of this consistency in both your own code, as well as politely pointing out violations in other people's code when doing their code reviews. While code prettiness should never be valued over launching or any user-visible impacting changes to the code, the idea is that maintaining a readable codebase helps things be more maintainable, and in the long run will make it easier to do the real changes that do make user-visible changes.

There may be lots of legacy files that do not adhere to the current style guide; if you're editing an old file, be consistent with what's around you.

To help adhere to these rules, some tools are available to automatically catch, and in some cases fix, style violations. See the per-language guides below for more info.

## TODOs

If there is something that you want to deal with later, it�s appropriate to mark it in code. There�s an advantage to using a standard format, which is this:

```
# TODO(your_username): Fix this to work with frobnozzes too
# TODO(your_username): Remove this once we support quxxes (at least by Dec 2012)
```

The text TODO is followed by your username in parentheses. This does not mean that you are on the hook to follow through on the TODO. Rather, it means that you are the person most knowledgeable about it, so if others run across the TODO and have questions about it, they know who to talk to.

In code reviews, it is common to put in TODOs when a reviewer points out some thing in the code that could be improved, but is not necessary to do right away.

## Linters

In order to more easily follow these style guides, "linters" can be integrated into your code editor to create warnings when these guidelines are not followed.
We use [ESLint](http://eslint.org/) and [PyLint](https://www.pylint.org/) to enforce these guidelines.

The [er2_web](https://bitbucket.org/erams2/er2_web) project automatically generates ***.eslintrc*** and ***.pylintrc*** files at the top of your project filesystem, preconfigured with these rules in mind.  To generate
these, run `python /path/to/er2_web/app/er2/deploy/start.py`.

Once these files have been generated, you can configure them to work with several common IDEs out of the box:

- [ESLint for Sublime](https://packagecontrol.io/packages/ESLint)
- [ESLint for Atom](https://atom.io/packages/linter-eslint)
- [ESLint for WebStorm](https://www.jetbrains.com/help/webstorm/eslint.html)
- [Pylint for Sublime](https://github.com/SublimeLinter/SublimeLinter-pylint)
- [Pylint for Atom](https://atom.io/packages/linter-pylint)


## Language Style Guides

- [JavaScript](/style/javascript.md)
- [React](/style/react.md)
- [CSS](/style/css.md)
- [Python](/style/python.md)
